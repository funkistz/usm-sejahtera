// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase_old: {
    apiKey: 'AIzaSyDyaa8zfLQSTT81CIbLrq_-Ow_iamxYfTs',
    authDomain: 'live-tracking-dd153.firebaseapp.com',
    databaseURL: 'https://live-tracking-dd153.firebaseio.com',
    projectId: 'live-tracking-dd153',
    storageBucket: 'live-tracking-dd153.appspot.com',
    messagingSenderId: '697145581280',
    appId: '1:697145581280:web:7cf95c1735d134e3207a85'
  },
  firebase: {
    apiKey: 'AIzaSyBeazdx_3lxLNAa1GVyeyn0eOMPh43wwBk',
    authDomain: 'usm-monitoring-system.firebaseapp.com',
    databaseURL: 'https://usm-monitoring-system.firebaseio.com',
    projectId: 'usm-monitoring-system',
    storageBucket: 'usm-monitoring-system.appspot.com',
    messagingSenderId: '1018117311324',
    appId: '1:1018117311324:web:70aec838fef6ced86b4f99'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
