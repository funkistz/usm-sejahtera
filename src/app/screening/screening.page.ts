import { Component, OnInit, ViewChild } from '@angular/core';
import { BluetoothSerial } from '@ionic-native/bluetooth-serial/ngx';
import { AlertController, ToastController, Platform, NavController } from '@ionic/angular';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';
import { interval } from 'rxjs';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

import { BluetoothService } from '../bluetooth';
import { LocationService, User } from 'src/app/services/location.service';
import { TrackerService } from 'src/app/services/tracker.service';
import { NavigationExtras } from '@angular/router';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { ActivatedRoute, Router } from '@angular/router';
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  Marker,
  GoogleMapsAnimation,
  MyLocation,
  LatLngBounds,
  Poly,
  LocationService as LocationServiceGoogle,
  LatLng,
} from '@ionic-native/google-maps';

@Component({
  selector: 'app-screening',
  templateUrl: './screening.page.html',
  styleUrls: ['./screening.page.scss'],
})
export class ScreeningPage implements OnInit {

  @ViewChild('content', { static: false }) private content: any;
  public screeningForm: FormGroup;
  user: any;
  platformType;
  formStatus = 'idle';
  currentCampus = null;

  campusFences = [
    {
      name: 'Main Campus',
      coordinate: [
        { lat: 5.3550, lng: 100.2931 },
        { lat: 5.3580, lng: 100.2931 },
        { lat: 5.3580, lng: 100.2880 },
        { lat: 5.3550, lng: 100.2880 },
      ],
      marker: null
    },
    {
      // name: 'RST (Main Campus)',
      name: 'Main Campus',
      coordinate: [
        { lat: 5.3520, lng: 100.3105 },
        { lat: 5.3630, lng: 100.3105 },
        { lat: 5.3630, lng: 100.2931 },
        { lat: 5.3520, lng: 100.2931 },
      ],
      marker: null
    },
    {
      // name: 'sains@usm',
      name: 'Main Campus',
      coordinate: [
        { lat: 5.338610, lng: 100.281504 },
        { lat: 5.340378, lng: 100.283274 },
        { lat: 5.341649, lng: 100.282249 },
        { lat: 5.340170, lng: 100.280082 },
      ],
      marker: null
    },
    {
      name: 'Engineering Campus',
      coordinate: [
        { lat: 5.1506, lng: 100.4845 },
        { lat: 5.158001, lng: 100.494427 },
        { lat: 5.148140, lng: 100.501572 },
        { lat: 5.140719, lng: 100.491543 },
      ],
      marker: null
    },
    {
      name: 'IPPT Campus',
      coordinate: [
        { lat: 5.5292, lng: 100.4752 },
        { lat: 5.5199, lng: 100.4752 },
        { lat: 5.5199, lng: 100.479638 },
        { lat: 5.5292, lng: 100.479638 },
      ],
      marker: null
    },
    {
      // name: 'sains@bertam',
      name: 'IPPT Campus',
      coordinate: [
        { lat: 5.515024, lng: 100.439905 },
        { lat: 5.515024, lng: 100.438869 },
        { lat: 5.515995, lng: 100.438869 },
        { lat: 5.515995, lng: 100.439905 },
      ],
      marker: null
    },
    {
      name: 'Health Campus',
      coordinate: [
        { lat: 6.094298, lng: 102.278710 },
        { lat: 6.101690, lng: 102.278710 },
        { lat: 6.101690, lng: 102.2920 },
        { lat: 6.094298, lng: 102.2920 },
      ],
      marker: null
    },
  ];

  constructor(
    private bluetoothSerial: BluetoothSerial,
    private alertController: AlertController,
    private diagnostic: Diagnostic,
    public toastController: ToastController,
    public bluetoothService: BluetoothService,
    public platform: Platform,
    private locationService: LocationService,
    public trackerService: TrackerService,
    public formBuilder: FormBuilder,
    public navCtrl: NavController,
    private geolocation: Geolocation,
    public router: Router,
  ) {

    this.screeningForm = formBuilder.group({
      screening_question1: new FormControl(null, Validators.required),
      screening_question2: new FormControl(null, Validators.required),
      fever: new FormControl(null, Validators.required),
      cough: new FormControl(null, Validators.required),
      sore_throat: new FormControl(null, Validators.required),
      running_nose: new FormControl(null, Validators.required),
      shortness_of_breath: new FormControl(null, Validators.required),
      temperature: new FormControl(null, Validators.required),
      agreement: new FormControl(false, Validators.requiredTrue),
    });

  }

  ngOnInit() {

    this.bluetoothService.diagnosticBluetooth(this);
    this.trackerService.diagnosticLocation(this);

  }

  scrollToBottomOnInit() {
    console.log(this.content);
    setTimeout(() => {
      this.content.scrollToBottom(300);
    }, 500);
  }

  ionViewDidEnter() {

    if (this.platform.is('android') || this.platform.is('desktop')) {
      this.platformType = 'android';
    } else {
      this.platformType = 'ios';
    }

    // console.log('control', this.screeningForm.controls);

  }

  onTermsChecked($event) {
    if (!$event.detail.checked) {
      this.screeningForm.patchValue({ agreement: false });
    } else {
      this.screeningForm.patchValue({ agreement: true });
    }
  }

  async presentToast(text) {
    const toast = await this.toastController.create({
      message: text,
      duration: 2000,
      position: 'top'
    });
    toast.present();
  }

  changeTemperature() {

  }

  checkCampus(myLocation) {

    console.log('myLocation', myLocation);
    let currentCampus = 'None';

    this.campusFences.forEach(campus => {

      console.log('campus', campus);
      if (Poly.containsLocation(myLocation, campus.coordinate)) {
        currentCampus = campus.name;
      }

    });

    return currentCampus;

  }

  async submitData(value) {

    console.log('submitData first', this.bluetoothService.user);

    const alert = await this.alertController.create({
      header: 'Confirmation',
      message: 'Are you sure to submit your data?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Submit',
          handler: () => {

            this.formStatus = 'loading';

            this.geolocation.getCurrentPosition().then((resp) => {

              value.latitude = resp.coords.latitude;
              value.longitude = resp.coords.longitude;

              // let myLocation = new LatLng(5.3569303, 100.3058593);
              let myLocation = new LatLng(resp.coords.latitude, resp.coords.longitude);
              value.campus = this.checkCampus(myLocation);

              console.log('campus: ', value.campus);
              // console.log('user', this.bluetoothService.user);
              // console.log('value', value);

              this.locationService.updateScreeningStatus(this.bluetoothService.user, value).then(() => {

                this.locationService.updateScreeningStatusUser(this.bluetoothService.user, value).then(() => {

                  this.formStatus = 'success';
                  setTimeout(() => {
                    this.formStatus = 'idle';
                  }, 5000);
                  this.presentToast('Your data has been updated.');
                  this.screeningForm.patchValue({ agreement: false });

                  for (let key in this.screeningForm.value) {
                    if (this.screeningForm.value.hasOwnProperty(key)) {
                      if (this.screeningForm.value[key] === true || this.screeningForm.value[key] >= 37.5) {
                        this.alertCovid();
                        break;
                      }
                    }
                  }

                  this.startTracking();

                }, err => {
                  console.log('updateScreeningStatus error: ', err);
                  this.presentToast('Error updating data.');
                  setTimeout(() => {
                    this.formStatus = 'idle';
                  }, 3000);
                });

              }, err => {
                console.log('updateScreeningStatus error: ', err);
                this.presentToast('Error updating data.');
                setTimeout(() => {
                  this.formStatus = 'idle';
                }, 3000);
              });

            }, error => {
              console.log('getCurrentPosition error: ', error);
              this.presentToast('Error getting location::: ' + error);
              setTimeout(() => {
                this.formStatus = 'idle';
              }, 3000);
            });

          }
        }
      ]
    });

    this.diagnostic.isLocationEnabled().then((success) => {

      if (success) {
        this.trackerService.locationOn = true;
        alert.present();
      } else {
        this.trackerService.locationOn = false;
      }
    }, (error) => {
      console.log(error);

      if (this.platform.is('desktop')) {
        this.trackerService.locationOn = true;
        alert.present();
      } else {
        this.trackerService.locationOn = false;
      }
    });

  }

  async alertCovid() {
    const alert = await this.alertController.create({
      mode: 'ios',
      header: 'Warning!',
      backdropDismiss: false,
      cssClass: 'alert-danger',
      message:
        'Kindly seek immediate attention at Pusat Sejahtera Main Campus/ Unit Kesihatan Engineering Campus/ IPPT/ HUSM . <br/><br/>Please confirm your health status before back to work.',
      buttons: ['Dismiss']
    });

    await alert.present();
  }

  startTracking() {

    const navigationExtras: NavigationExtras = {
      queryParams: {
        tracking: true
      }
    };
    this.router.navigateByUrl('tabs/tracker', navigationExtras);

  }

}
