import { Component, AfterContentInit, OnInit, ViewChild, ElementRef } from '@angular/core';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';
import {
  BackgroundGeolocation,
  BackgroundGeolocationConfig,
  BackgroundGeolocationResponse,
  BackgroundGeolocationEvents
} from '@ionic-native/background-geolocation/ngx';
import { HTTP } from '@ionic-native/http/ngx';
import { Platform } from '@ionic/angular';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { ActivatedRoute, Router } from '@angular/router';
import { LocationService, User } from 'src/app/services/location.service';
import { Observable } from 'rxjs';
import { getLocaleDateTimeFormat } from '@angular/common';
import { ToastController, NavController } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { PhoneValidator } from '../validators/phone';
import { LoadingController } from '@ionic/angular';
import { BluetoothService } from '../bluetooth';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';
import { interval } from 'rxjs';
import { BackgroundMode } from '@ionic-native/background-mode/ngx';
import { Storage } from '@ionic/storage';
import { AgeValidator } from '../validators/age';
import { ICValidator } from '../validators/ic';
import { ModalController } from '@ionic/angular';
import { DepartmentsPage } from './../departments/departments.page';

declare var google;
declare var navigator: any;

@Component({
  selector: 'app-tracker',
  templateUrl: 'tracker.page.html',
  styleUrls: ['tracker.page.scss'],
})
export class TrackerPage {

  locationOn: any = false;
  isTracking = false;
  error_messages = {
    name: [
      { type: 'required', message: 'Name is required.' },
      { type: 'maxlength', message: 'Maximum character is 80.' },
    ],
    department: [
      { type: 'required', message: 'Department is required.' },
    ],
    phone: [
      { type: 'required', message: 'Phone no. is required.' },
      { type: 'notvalid', message: 'Phone no. not valid.' }
    ],
    email: [
      { type: 'required', message: 'Email is required.' },
      { type: 'notvalid', message: 'Please enter a valid email address.' },
      { type: 'usm', message: 'Please enter USM email address.' }
    ],
    ic: [
      { type: 'required', message: 'Field is required.' },
      { type: 'NaN', message: 'Please enter number only.' },
      { type: 'icnotvalid', message: 'Please enter a valid IC.' },
      { type: 'passportnotvalid', message: 'Please enter a valid passport.' },
    ],
    age: [
      { type: 'required', message: 'Age is required.' },
      { type: 'NaN', message: 'Please enter number only.' },
      { type: 'maxmin', message: 'Age is not valid.' },
    ],
  };

  locationCoords: any;
  timetest: any;
  map;
  // @ViewChild('mapElement', { static: false }) mapNativeElement: ElementRef;

  public watch: any;
  latitude: any;
  longitude: any;
  location: any = {
    lat: 0,
    lng: 0
  };
  syncDate: any = null;
  loading;
  firstTime: any = true;
  isActivePage = true;
  checkingBL;
  editMode = false;
  currentID = 'ic'; // else passport

  formIC: FormControl = new FormControl('', Validators.compose([
    Validators.required,
    ICValidator.isValid
  ]));

  formPassport: FormControl = new FormControl('', Validators.compose([
    Validators.required,
    ICValidator.isValidPassport
  ]));

  user: User = {
    id: null,
    name: null,
    department: null,
    email: null,
    phone: null,
    type: null,
    location: null,
    first_screening_date: null,
    last_screening_date: null,
    temperature: null,
    ic: null,
    age: null,
    last_screening_campus: '',
    last_update: null
  };
  lastScreeningDate: any;
  public registerForm: FormGroup;

  constructor(
    private androidPermissions: AndroidPermissions,
    private geolocation: Geolocation,
    private locationAccuracy: LocationAccuracy,
    private backgroundGeolocation: BackgroundGeolocation,
    private http: HTTP,
    public platform: Platform,
    private nativeStorage: NativeStorage,
    public router: Router,
    private locationService: LocationService,
    private route: ActivatedRoute,
    public toastController: ToastController,
    public formBuilder: FormBuilder,
    public loadingController: LoadingController,
    public bluetoothService: BluetoothService,
    private navCtrl: NavController,
    private diagnostic: Diagnostic,
    private backgroundMode: BackgroundMode,
    private storage: Storage,
    public modalController: ModalController,
  ) {

    // this.diagnosticLocation();
    this.firstTime = true;

    this.location = {
      lat: 0,
      lng: 0
    };

    this.registerForm = formBuilder.group({
      name: new FormControl('', Validators.compose([
        Validators.required,
        Validators.maxLength(30),
      ])),
      department: new FormControl('', Validators.compose([
        Validators.required,
      ])),
      phone: new FormControl('', Validators.compose([
        Validators.required,
        PhoneValidator.isValid
      ])),
      ic: new FormControl('', Validators.compose([
        Validators.required,
        ICValidator.isValid
      ])),
      age: new FormControl('', Validators.compose([
        AgeValidator.isValid,
      ])),
      last_screening_date: new FormControl('', null),
      temperature: new FormControl('', null),
    });

    this.platform.ready().then(() => {

      if (this.platform.is('desktop')) {

        console.log('is desktop');

        this.storage.get('user_email').then(user_email => {

          console.log('email', user_email);

          if (user_email) {
            this.user.id = user_email;
            this.initFirstEnter();
          } else {
            this.logout();
          }

        }, error => {
          console.error(error);
          this.logout();
        });

      } else {

        this.route.queryParams.subscribe((res) => {

          if (res.email) {
            this.user.id = res.email;
          }

        });

        this.initFirstEnter();

      }

      this.platform.pause.subscribe(e => {
        if (this.user.id) {
          this.locationService.updateAppStatus(this.user, e);
        }
      });
    });

  }

  ionViewDidEnter(): void {

    this.route.queryParams.subscribe((res) => {

      if (res.tracking && !this.isTracking) {
        this.isTracking = true;
        this.initLocation();
      }

    });

    if (this.firstTime || this.bluetoothService.user === null) {

      this.firstTime = false;

    } else {

      this.locationService.getUser(this.user.id).subscribe(user => {

        user.id = this.user.id;

        console.log('user ionViewDidEnter', user);
        this.assignFormValue(user);
      }, error => {
        console.log('user not exist');
      });
    }

  }

  initFirstEnter() {

    this.presentLoading();

    this.locationService.getUser(this.user.id).subscribe(user => {

      console.log('user initFirstEnter', user);
      this.assignFormValue(user);
      this.dismissLoading();

      if (!user.last_update || !user.department) {
        this.enableEditMode();
        this.registerForm.controls.department.setValue('');
        this.departmentsModal();
      }

    }, error => {
      this.dismissLoading();
      console.log('user not exist');
    });

  }

  ionViewDidLeave() {
    this.isActivePage = false;
  }

  ionViewWillEnter() {
    this.isActivePage = true;
  }

  diagnosticLocation() {
    this.locationOn = false;
    // in 3 seconds do something
    this.diagnostic.isLocationAvailable().then(this.locationSuccess, this.locationFail);
  }

  locationSuccess = (data) => {

    this.checkLocationState();
    this.checkingBL = interval(5000).subscribe(x => {
      if (this.isActivePage) {
        this.checkLocationState();
      }
    });

    this.platform.pause.subscribe(async () => {
      this.checkingBL.unsubscribe();
    });

    this.platform.resume.subscribe(async () => {
      this.checkLocationState();
      this.checkingBL = interval(5000).subscribe(x => {
        if (this.isActivePage) {
          this.checkLocationState();
        }
      });
    });
  }
  locationFail = (error) => {
    console.log(error);
    if (this.platform.is('desktop')) {
      this.locationOn = true;
    }
  }

  checkLocationState() {

    console.log('checking location state...');
    this.diagnostic.isLocationEnabled().then((success) => {

      if (success) {
        this.locationOn = true;
      } else {
        this.locationOn = false;
      }
    }, (error) => {
      console.log(error);
      this.locationOn = false;
    });
  }

  assignFormValue(user) {

    console.log('user assignFormValue', user);

    this.user = user;
    this.lastScreeningDate = user.last_screening_date;
    this.bluetoothService.user = this.user;
    if (user.name) {
      this.registerForm.controls.name.setValue(user.name);
    }
    if (user.phone) {
      this.registerForm.controls.phone.setValue(user.phone);
    }
    if (user.department) {
      this.registerForm.controls.department.setValue(user.department);
    }
    if (user.temperature) {
      this.registerForm.controls.temperature.setValue(user.temperature);
    }
    if (user.last_screening_date) {
      this.registerForm.controls.last_screening_date.setValue(user.last_screening_date);
    }
    if (user.age) {
      this.registerForm.controls.age.setValue(user.age);
    }
    if (user.ic) {
      this.registerForm.controls.ic.setValue(user.ic);
    }

  }

  dateToString(date) {

  }

  // 10 sec for now
  isHourAgo() {

    if (!this.syncDate) {
      console.log('date not exist');
      return false;
    }

    // temporarily use minute
    // const HOUR = 1000 * 60 * 60;
    const HOUR = 1000 * 5 * 1;
    const anHourAgo = Date.now() - HOUR;

    console.log('date', this.syncDate);
    return this.syncDate > anHourAgo;
  }

  startBackgroundGeolocation() {

    console.log('startBackgroundGeolocation...');

    const config: BackgroundGeolocationConfig = {
      desiredAccuracy: 10,
      stationaryRadius: 5,
      distanceFilter: 5,
      debug: false, //  enable this hear sounds for background-geolocation life-cycle.
      stopOnTerminate: false,

      // android
      notificationsEnabled: true,
      notificationTitle: 'Status',
      notificationText: 'Monitoring',
      notificationIconLarge: 'icon', // Android
      notificationIconSmall: 'icon', // Android
      startOnBoot: false,
      startForeground: true,
      interval: 7000,
      fastestInterval: 5000,
      activitiesInterval: 7000,

      // ios
      // activityType: 'AutomotiveNavigation',
      // saveBatteryOnBackground: true
    };

    this.backgroundGeolocation.configure(config).then(() => {
      this.backgroundGeolocation
        .on(BackgroundGeolocationEvents.location)
        .subscribe((location: BackgroundGeolocationResponse) => {
          console.log('update background geolocation', location);
          // this.presentToast('update background geolocation: ' + location.latitude + '|' + location.longitude);

          this.location.lat = location.latitude;
          this.location.lng = location.longitude;

          this.sendGPS(this.location);

          // IMPORTANT:  You must execute the finish method here to inform the native plugin that you're finished,
          // and the background-task may be completed.  You must do this regardless if your operations are successful or not.
          // IF YOU DON'T, ios will CRASH YOUR APP for spending too much time in the background.
        });
    });

    // start background mode
    this.platform.ready().then(() => {
      this.backgroundMode.enable();
      this.backgroundMode.on('activate').subscribe(() => {
        this.backgroundMode.disableWebViewOptimizations();
      });
      this.backgroundMode.disableBatteryOptimizations();
      this.backgroundMode.on('deactivate').subscribe(() => {
      });
    });
    // start recording location
    this.backgroundGeolocation.start();

    // If you wish to turn OFF background-tracking, call the #stop method.
    // this.backgroundGeolocation.stop();
  }

  startGeolocation() {

    const options = {
      frequency: 3000,
      enableHighAccuracy: true
    };

    // Add watch
    this.watch = navigator.geolocation.watchPosition((position) => {
      // do something with position
      console.log('update geolocation');

      this.location.lat = position.coords.latitude;
      this.location.lng = position.coords.longitude;

      this.sendGPS(this.location);

    }, (error) => {
      // do something with error
      console.log('error start geolocation', error);
    }, options);

    // this.watch = navigator.geolocation.watchPosition(options);
    // this.watch.subscribe((position) => {

    //   console.log('update geolocation');

    //   this.location.lat = position.coords.latitude;
    //   this.location.lng = position.coords.longitude;

    //   this.sendGPS(this.location);

    // });

  }

  sendGPS(location) {

    console.log('update location');
    // this.presentToast('update location');

    // dont update if less than 1 hour
    if (this.isHourAgo()) {
      // this.presentToast('Location found.');
      this.backgroundGeolocation.finish();
      return;
    }

    this.locationService.updateLocation(this.user, location).then(() => {

      // this.presentToast('Location updated.');
      this.syncDate = Date.now();
      this.getUser();

      this.locationService.addUserLocations(this.user, location).then(() => {
        this.backgroundGeolocation.finish();
      }, err => {
        console.log(err);
        this.backgroundGeolocation.finish();
      });

    }, err => {
      console.log(err);
      this.backgroundGeolocation.finish();
    });



  }

  getUser() {
    this.locationService.getUser(this.user.id).subscribe(user => {

      user.id = this.user.id;

      this.user = user;
      this.bluetoothService.user = this.user;

      if (user.name) {
        this.registerForm.controls.name.setValue(user.name);
      }
      if (user.phone) {
        this.registerForm.controls.phone.setValue(user.phone);
      }
      if (user.department) {
        this.registerForm.controls.department.setValue(user.department);
      }

    },
      error => {
        console.log('user not exist');
        this.locationService.addOrUpdateUser(this.user);
      });
  }

  initLocation() {

    console.log('initLocation');

    this.platform.ready().then(() => {

      this.syncDate = null;

      this.backgroundGeolocation.checkStatus().then((status) => {
        console.log('geo status', status);
        if (!status.isRunning) {
          this.startBackgroundGeolocation();
        }
      }).catch((error) => {
        console.log('Error checkStatus.', error);
      });

      this.geolocation.getCurrentPosition().then((resp) => {
        console.log('getCurrentPosition');

        this.location.lat = resp.coords.latitude;
        this.location.lng = resp.coords.longitude;

        // const map = new google.maps.Map(this.mapNativeElement.nativeElement, {
        //   center: { lat: -34.397, lng: 150.644 },
        //   zoom: 16
        // });

        // const infoWindow = new google.maps.InfoWindow;

        // infoWindow.setPosition(this.location);
        // infoWindow.setContent('Location found.');
        // infoWindow.open(map);
        // map.setCenter(this.location);

        this.sendGPS(this.location);

      }).catch((error) => {
        console.log('Error getting location.', error);
      });

      if (!this.watch) {
        // this.startGeolocation();
      }

    });

  }

  // Check if application having GPS access permission
  checkGPSPermission() {
    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(
      result => {
        if (result.hasPermission) {

          // If having permission show 'Turn On GPS' dialogue
          this.askToTurnOnGPS();
        } else {

          // If not having permission ask for permission
          this.requestGPSPermission();
        }
      },
      err => {
        alert(err);
      }
    );
  }

  requestGPSPermission() {
    this.locationAccuracy.canRequest().then((canRequest: boolean) => {
      if (canRequest) {
        console.log('4');
      } else {
        // Show 'GPS Permission Request' dialogue
        this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION)
          .then(
            () => {
              // call method to turn on GPS
              this.askToTurnOnGPS();
            },
            error => {
              // Show alert if user click on 'No Thanks'
              alert('requestPermission Error requesting location permissions ' + error);
            }
          );
      }
    });
  }

  askToTurnOnGPS() {
    this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
      () => {
        // When GPS Turned ON call method to get Accurate location coordinates
        this.getLocationCoordinates();
      },
      error => alert('Error requesting location permissions ' + JSON.stringify(error))
    );
  }

  // Methos to get device accurate coordinates using device GPS
  getLocationCoordinates() {
    this.geolocation.getCurrentPosition().then((resp) => {
      this.locationCoords.latitude = resp.coords.latitude;
      this.locationCoords.longitude = resp.coords.longitude;
      this.locationCoords.accuracy = resp.coords.accuracy;
      this.locationCoords.timestamp = resp.timestamp;
    }).catch((error) => {
      alert('Error getting location' + error);
    });
  }

  logout() {

    this.locationService.updateAppStatus(this.user.id, 'logout').then(() => {

    }, err => {
      console.log(this.user.id, err);
    });

    this.firstTime = true;

    this.nativeStorage.clear().then(
      () => {

        console.log('storage cleared');

        if (this.watch) {
          navigator.geolocation.clearWatch(this.watch);
        }

        this.backgroundGeolocation.checkStatus().then((status) => {
          console.log('geo status', status);
          if (status.isRunning) {
            this.backgroundGeolocation.finish();

            // disable background mode
            this.platform.ready().then(() => {
              if (this.backgroundMode.isEnabled()) {
                this.backgroundMode.disable();
              }
            });
            this.backgroundGeolocation.stop();
          }
        });

        this.bluetoothService.user = null;
        this.navCtrl.navigateRoot(['/login']);

      }, error => console.error('Error storing item', error));

    this.bluetoothService.user = null;
    this.navCtrl.navigateRoot(['/login']);


  }

  findDevice() {
    this.router.navigate(['/tracker/bluetooth']);
  }

  updateUser(value) {

    this.registerForm.markAllAsTouched();
    if (!this.registerForm.valid) {

      this.presentToast('Please fill all input.');
      return;
    }

    this.user.name = value.name;
    this.user.phone = value.phone;
    this.user.ic = value.ic;
    this.user.department = value.department;
    this.user.age = value.age;

    this.presentLoading();
    this.locationService.updateUser(this.user).then(() => {
      this.dismissLoading();
      this.presentToast('Successfully updated.');
      this.enableEditMode();
    }, err => {
      this.dismissLoading();
      this.presentToast(err.message);
      this.enableEditMode();
      console.log('Error updating user.');
    });
  }

  async presentToast(text) {
    const toast = await this.toastController.create({
      message: text,
      mode: 'ios',
      duration: 2000,
      position: 'top'
    });
    toast.present();
  }

  async presentLoading() {
    this.loading = await this.loadingController.create({
      message: 'Please wait...'
    });
    await this.loading.present();
  }

  dismissLoading() {

    setTimeout(() => {
      if (this.loading) {
        this.loading.dismiss();
      }
    }, 500);

  }

  enableEditMode() {
    this.editMode = !this.editMode;
  }

  switchPassport() {
    this.registerForm.removeControl('ic');
    this.registerForm.addControl('ic', this.formPassport);
    this.currentID = 'passport';
  }

  switchIC() {
    this.registerForm.removeControl('ic');
    this.registerForm.addControl('ic', this.formIC);
    this.currentID = 'ic';
  }

  async departmentsModal() {

    let modal = await this.modalController.create({
      component: DepartmentsPage
    });

    await modal.present();

    const data: any = await modal.onDidDismiss();
    console.log(data);
    if (data.data) {
      this.registerForm.controls.department.setValue(data.data.name);
    }

  }

}
