import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { StartupPageRoutingModule } from './startup-routing.module';

import { StartupPage } from './startup.page';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { ComponentsModule } from '../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    StartupPageRoutingModule,
    ComponentsModule,
  ],
  declarations: [StartupPage]
})
export class StartupPageModule { }
