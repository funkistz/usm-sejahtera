import { Component, OnInit } from '@angular/core';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { ActivatedRoute, Router } from '@angular/router';
import { Platform } from '@ionic/angular';
import { LocationService, User } from 'src/app/services/location.service';
import { NavController } from '@ionic/angular';
import { NavigationExtras } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { EmailValidator } from '../validators/email';
import { PhoneValidator } from '../validators/phone';
import { ICValidator } from '../validators/ic';
import { AgreementValidator } from '../validators/agreement';
import { PasswordValidator } from '../validators/password';
import { AgeValidator } from '../validators/age';
import { AuthenticateService } from '../services/authentication.service';
import { LoadingController } from '@ionic/angular';
import { ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { ModalController } from '@ionic/angular';
import { DepartmentsPage } from './../departments/departments.page';

@Component({
  selector: 'app-startup',
  templateUrl: './startup.page.html',
  styleUrls: ['./startup.page.scss'],
})
export class StartupPage implements OnInit {

  error_messages = {
    name: [
      { type: 'required', message: 'Name is required.' },
      { type: 'maxlength', message: 'Maximum character is 80.' },
    ],
    department: [
      { type: 'required', message: 'Department is required.' },
    ],
    phone: [
      { type: 'required', message: 'Phone no. is required.' },
      { type: 'notvalid', message: 'Phone no. not valid.' }
    ],

    email: [
      { type: 'required', message: 'Email is required.' },
      { type: 'notvalid', message: 'Please enter a valid email address.' },
      { type: 'usm', message: 'Please enter USM email address.' }
    ],

    ic: [
      { type: 'required', message: 'Field is required.' },
      { type: 'NaN', message: 'Please enter number only.' },
      { type: 'icnotvalid', message: 'Please enter a valid IC.' },
      { type: 'passportnotvalid', message: 'Please enter a valid passport.' },
    ],

    age: [
      { type: 'required', message: 'Age is required.' },
      { type: 'NaN', message: 'Please enter number only.' },
      { type: 'maxmin', message: 'Age is not valid.' },
    ],
    reason_back_to_campus: [
      { type: 'required', message: 'Reason back to campus is required.' },
      { type: 'maxlength', message: 'Maximum character is 100.' }
    ],

    password: [
      { type: 'required', message: 'password is required.' },
      { type: 'minlength', message: 'Minimum character is 6.' },
      { type: 'maxlength', message: 'Maximum character is 30.' }
    ],
    confirm_password: [
      { type: 'required', message: 'Password is required.' },
      { type: 'minlength', message: 'Minimum character is 6.' },
      { type: 'maxlength', message: 'Maximum character is 30.' },
    ],
  };

  userID: any = '';
  userPass: any;
  private user: User = null;
  loading: any;
  isLogin = true;
  isReset = false;
  isRegister = false;
  currentID = 'ic'; // else passport
  emailVerify = false;
  sentTimestamp;
  canSendEmail = true;

  public loginForm: FormGroup;
  public resetPasswordForm: FormGroup;
  public registerForm: FormGroup;
  public screeningForm: FormGroup;

  formReasonBackToCampus: FormControl = new FormControl('', Validators.compose([
    Validators.required,
    Validators.maxLength(100),
  ]));

  formPregnant: FormControl = new FormControl('no', Validators.compose([
    Validators.required,
  ]));

  formStudentCategory: FormControl = new FormControl('diploma', Validators.compose([
    Validators.required,
  ]));

  formIC: FormControl = new FormControl('', Validators.compose([
    Validators.required,
    ICValidator.isValid
  ]));

  formPassport: FormControl = new FormControl('', Validators.compose([
    Validators.required,
    ICValidator.isValidPassport
  ]));

  constructor(
    private nativeStorage: NativeStorage,
    public router: Router,
    public platform: Platform,
    public navCtrl: NavController,
    public formBuilder: FormBuilder,
    private authService: AuthenticateService,
    public loadingController: LoadingController,
    public toastController: ToastController,
    private locationService: LocationService,
    private storage: Storage,
    public modalController: ModalController,
  ) {

    this.loginForm = formBuilder.group({
      email: new FormControl('', Validators.compose([
        Validators.required,
        EmailValidator.isValid
      ])),
      password: new FormControl('', Validators.compose([
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(30)
      ])),
    });

    this.resetPasswordForm = formBuilder.group({
      email: new FormControl('', Validators.compose([
        Validators.required,
        EmailValidator.isValid
      ]))
    });

    this.registerForm = formBuilder.group({
      type: new FormControl('student', Validators.compose([
        Validators.required,
      ])),
      name: new FormControl('', Validators.compose([
        Validators.required,
        Validators.maxLength(200),
      ])),
      student_category: this.formStudentCategory,
      department: new FormControl('', Validators.compose([
        Validators.required,
        Validators.maxLength(150),
      ])),
      email: new FormControl('', Validators.compose([
        Validators.required,
        EmailValidator.isValid
      ])),
      sex: new FormControl('male', Validators.compose([
        Validators.required,
      ])),
      ic: new FormControl('', Validators.compose([
        Validators.required,
        ICValidator.isValid
      ])),
      age: new FormControl('', Validators.compose([
        AgeValidator.isValid,
      ])),
      phone: new FormControl('', Validators.compose([
        Validators.required,
        PhoneValidator.isValid
      ])),
      pregnant: this.formPregnant,
      password: new FormControl('', Validators.compose([
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(30)
      ])),
      confirm_password: new FormControl('', Validators.compose([
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(30)
      ])),
      question1: new FormControl(null, Validators.required),
      question2: new FormControl(null, Validators.required),
      question3: new FormControl(null, Validators.required),
      question4: new FormControl(null, Validators.required),
      question5: new FormControl(null, Validators.required),
      question6: new FormControl(null, Validators.required),
      chronic_disease: new FormControl(null, Validators.required),
      agreement: new FormControl(false, Validators.requiredTrue),
    }, {
      validators: this.password.bind(this)
    });

  }

  changeForm(type) {

    if (type === 'student') {
      this.switchStudentForm();
      console.log(this.registerForm);

    } else {
      this.switchStaffForm();
      console.log(this.registerForm);

    }
  }

  switchStudentForm() {
    this.registerForm.addControl('student_category', this.formStudentCategory);
  }

  switchStaffForm() {
    this.registerForm.removeControl('student_category');
  }

  switchPassport() {
    this.registerForm.removeControl('ic');
    this.registerForm.addControl('ic', this.formPassport);
    this.currentID = 'passport';
  }

  switchIC() {
    this.registerForm.removeControl('ic');
    this.registerForm.addControl('ic', this.formIC);
    this.currentID = 'ic';
  }

  onTermsChecked($event) {
    console.log($event.detail);
    if (!$event.detail.checked) {
      this.registerForm.patchValue({ agreement: false });
    } else {
      this.registerForm.patchValue({ agreement: true });
    }
  }

  async departmentsModal() {

    let modal = await this.modalController.create({
      component: DepartmentsPage
    });

    await modal.present();

    const data: any = await modal.onDidDismiss();
    console.log(data);
    if (data.data) {
      this.registerForm.controls.department.setValue(data.data.name);
    }

  }

  departmentsPage() {
    this.navCtrl.navigateForward('/departments');
  }

  ngOnInit() {

    this.platform.ready().then(() => {
      this.nativeStorage.getItem('user_email').then(user_email => {

        this.presentLoading();
        this.trackerPage(user_email);

      }, error => {
        console.error(error);
      });
    });

  }

  trackerPage(email) {

    if (email) {

      this.nativeStorage.setItem('user_email', email);
      this.storage.set('user_email', email);

      this.locationService.updateAppStatus(email, 'logged').then(() => {

        this.dismissLoading();
        this.presentToast('Login success');
        const navigationExtras: NavigationExtras = {
          queryParams: {
            email
          }
        };
        this.navCtrl.navigateRoot('tabs/tracker', navigationExtras);

      }, err => {
        console.log(err);
        this.dismissLoading();
        this.presentToast(err);
      });
    }

  }

  loginUser(value) {

    this.presentLoading();
    this.authService.loginUser(value)
      .then(res => {
        this.dismissLoading();
        console.log(this.authService.isEmailVerified());

        if (this.authService.isEmailVerified() || value.email == 'syimir@usm.my') {
          this.trackerPage(value.email);
        } else {

          // testing
          // this.trackerPage(value.email);

          this.presentToast('Email not verified, please verify your email first.', 'top', 'danger', 2500);
          this.emailVerify = true;
        }

      }, err => {
        this.dismissLoading();
        this.presentToast(err.message);
        console.log(err);
      });
  }

  sendVerificationEmail() {

    this.presentLoading();
    this.authService.sendVerificationEmail()
      .then(res => {
        this.dismissLoading();
        console.log(res);
        this.sentTimestamp = new Date();
        this.canSendEmail = false;

        setTimeout(() => {
          this.canSendEmail = true;
        }, 5000);

      }, err => {
        this.dismissLoading();
        this.presentToast(err.message);
        console.log(err);
      });
  }

  resetPassword(value) {

    // console.log(value);
    // return;

    this.presentLoading();
    this.authService.sendReset(value)
      .then(res => {
        this.presentToast('A password reset email has been send to your email.', 'top', 'success', 5000);

        console.log(res);
        this.dismissLoading();
        this.loginPage();
        this.resetPasswordForm.patchValue({ email: '' });

      }, err => {
        this.presentToast('Some error occured.');
        setTimeout(() => {
          this.dismissLoading();
        }, 1000);
        console.log(err);
      });
  }

  registerUser(value) {

    this.presentLoading();
    this.authService.registerUser(value)
      .then(res => {
        console.log('statrup res', res);
        this.dismissLoading();
        this.presentToast('Successfully registered, Please verify your email address before login.', 'top', 'warning', 4000);
        this.loginPage();

        this.sendVerificationEmail();
      }, err => {
        this.dismissLoading();
        this.presentToast(err.message);
        console.log(err);
      });
  }

  password(formGroup: FormGroup) {
    const { value: password } = formGroup.get('password');
    const { value: confirmPassword } = formGroup.get('confirm_password');
    return password === confirmPassword ? null : { passwordNotMatch: true };
  }

  async presentLoading() {
    this.loading = await this.loadingController.create({
      message: 'Please wait...'
    });
    await this.loading.present();
  }

  dismissLoading() {
    if (this.loading) {
      this.loading.dismiss();
    }
  }

  async presentToast(text, position = 'top', color = 'light', duration = 2000) {

    let toast;
    if (position === 'top') {
      toast = await this.toastController.create({
        message: text,
        mode: 'ios',
        duration,
        position: 'top',
        color,
      });
    } else if (position === 'middle') {
      toast = await this.toastController.create({
        message: text,
        mode: 'ios',
        duration,
        position: 'middle',
        color,
      });
    } else {
      toast = await this.toastController.create({
        message: text,
        mode: 'ios',
        duration,
        position: 'bottom',
        color,
      });
    }
    toast.present();
  }

  loginPage() {
    this.isReset = false;
    this.isRegister = false;
    this.isLogin = true;
  }

  registerPage() {
    this.isReset = false;
    this.isRegister = true;
    this.isLogin = false;
  }

  resetPage() {
    this.isLogin = false;
    this.isRegister = false;
    this.isReset = true;
  }

  faqPage() {
    this.navCtrl.navigateForward('/faq');
  }
}
