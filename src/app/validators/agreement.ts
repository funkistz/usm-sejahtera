import { FormControl } from '@angular/forms';

export class AgreementValidator {

    static isValid(control: FormControl): any {

        console.log(control);
        if (!control.value) {
            return {
                notvalid: true
            };
        }

        return null;
    }

}
