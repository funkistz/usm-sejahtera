import { FormControl } from '@angular/forms';

export class EmailValidator {

    static isValid(control: FormControl): any {

        // tslint:disable-next-line: max-line-length
        const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        if (!re.test(control.value) && control.value.length > 0) {
            return {
                notvalid: true
            };
        }

        if (!control.value.endsWith('usm.my') && control.value.length > 0) {

            return {
                usm: true
            };
        }

        return null;
    }

}
