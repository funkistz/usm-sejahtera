import { FormControl } from '@angular/forms';

export class AgeValidator {

    static isValid(control: FormControl): any {

        if (isNaN(control.value)) {
            return {
                NaN: true
            };
        }

        if (control.value && (Number(control.value) < 18 || Number(control.value) > 100)) {
            return {
                maxmin: true
            };
        }

        return null;
    }

}
