import { FormControl } from '@angular/forms';

export class PhoneValidator {

    static isValid(control: FormControl): any {

        // tslint:disable-next-line: max-line-length
        const re = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;

        if (!re.test(control.value) && control.value.length > 0) {
            return {
                notvalid: true
            };
        }

        return null;
    }

}
