import { FormControl } from '@angular/forms';

export class ICValidator {

    static isValid(control: FormControl): any {

        if (isNaN(control.value)) {
            return {
                NaN: true
            };
        }

        if (control.value && control.value.toString().length !== 12) {
            return {
                icnotvalid: true
            };
        }

        return null;
    }

    static isValidPassport(control: FormControl): any {

        if (control.value && control.value.toString().length < 4 || control.value.toString().length > 14) {
            return {
                passportnotvalid: true
            };
        }

        return null;
    }

}
