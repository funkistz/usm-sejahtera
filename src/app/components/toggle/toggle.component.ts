import { Component, OnInit, Input } from '@angular/core';
import { FormGroupDirective, ControlContainer } from '@angular/forms';

@Component({
  selector: 'app-toggle',
  templateUrl: './toggle.component.html',
  styleUrls: ['./toggle.component.scss'],
  viewProviders: [
    {
      provide: ControlContainer,
      useExisting: FormGroupDirective
    }
  ]
})
export class ToggleComponent implements OnInit {

  @Input() form;
  @Input() type;
  @Input() name;
  @Input() value;
  @Input() control;

  yesStatus = 'untoggle';
  noStatus = 'untoggle';

  constructor() { }

  ngOnInit() {
    if (this.control.value === true) {
      this.clickYes();
    } else if (this.control.value === false) {
      this.clickNo();
    }
  }

  ngAfterViewInit() {
    // You should see the actual form control properties being passed in
    // console.log('control value', this.control);
  }

  clickYes() {
    this.control.setValue(true);
    this.yesStatus = 'primary';
    this.noStatus = 'untoggle';
  }

  clickNo() {
    this.control.setValue(false);
    this.yesStatus = 'untoggle';
    this.noStatus = 'primary';
  }

}
