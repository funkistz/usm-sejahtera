import { Component } from '@angular/core';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { NavController } from '@ionic/angular';
import { BluetoothService } from '../bluetooth';

@Component({
  selector: 'app-faq',
  templateUrl: 'faq.page.html',
  styleUrls: ['faq.page.scss'],
})
export class FaqPage {

  constructor(
    private callNumber: CallNumber,
    public bluetoothService: BluetoothService,
    public navCtrl: NavController,
  ) {

    // if (!this.bluetoothService.user) {
    //   this.navCtrl.navigateRoot('tabs/tracker');
    // }

  }

  callNow(phoneNumber) {

    window.open(`tel:${phoneNumber}`, '_system');

    // this.callNumber.callNumber(phoneNumber, true)
    //   .then(res => console.log('Launched dialer!', res))
    //   .catch(err => console.log('Error launching dialer', err));
  }

}
