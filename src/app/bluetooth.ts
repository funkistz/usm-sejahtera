import { Injectable, NgZone } from '@angular/core';
import { BluetoothSerial } from '@ionic-native/bluetooth-serial/ngx';
import { AlertController, ToastController, Platform, LoadingController } from '@ionic/angular';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';
import { interval } from 'rxjs';
import { LocationService, User } from 'src/app/services/location.service';
import { BLE } from '@ionic-native/ble/ngx';

@Injectable({
    providedIn: 'root'
})
export class BluetoothService {

    loading;
    bluetoothOn: any = false;
    BLEdevices: any = [];
    peripheral: any;
    isFinishScanning = false;
    isScanning = false;

    unpairedDevices: any = [];
    pairedDevices: any = [];
    scannedDevice: any = false;
    gettingDevices: boolean;
    gettingPairedDevices: boolean;
    log: any = '';
    log2: any = '';
    statusMessage: any = '';
    data: any = {
        log: 'Logging start'
    };
    user: User;
    scannerResult: any = {
        body_temperature: null,
        ambient_temperature: null,
        scanner_distance: null,
    };

    question: any = {
        fever: false,
        cough: false,
        sore_throat: false,
        running_nose: false,
        shortness_of_breath: false,
        temperature: 0,
    };
    public parentPage;

    constructor(
        private bluetoothSerial: BluetoothSerial,
        private alertController: AlertController,
        private diagnostic: Diagnostic,
        public toastController: ToastController,
        public platform: Platform,
        public loadingController: LoadingController,
        private ble: BLE,
        private ngZone: NgZone,
    ) { }

    writeLog(log) {
        this.data.log += '<br>' + log;
    }
    writeLog2(log) {
        this.data.log2 += '<br>' + log;
    }
    resetLog() {
        this.data.log = '';
    }

    diagnosticBluetooth(parentPage) {
        this.parentPage = parentPage;
        this.bluetoothOn = false;
        if (this.platform.is('desktop')) {
            this.bluetoothOn = true;
        } else {
            this.diagnostic.isBluetoothAvailable().then(this.diagnoseSuccess, this.diagnoseFail);
        }
    }
    diagnosticBluetoothOnce() {
        this.bluetoothOn = false;

        if (this.platform.is('desktop')) {
            this.bluetoothOn = true;
        } else {
            this.diagnostic.isBluetoothAvailable().then(this.diagnoseSuccess, this.diagnoseFail);
        }
    }
    diagnoseSuccess = (data) => {
        this.checkBluetoothState();
        const intervalBL = interval(3000).subscribe(x => {

            console.log('checking bluetooth...');

            if (this.bluetoothOn === false) {
                this.checkBluetoothState();
            }
        });
    }
    diagnoseFail = (error) => {
        console.error(error);
        this.presentToast(error, 'top', 'danger');
    }
    checkBluetoothState() {
        console.log('checking bluetooth state...');
        this.diagnostic.getBluetoothState().then((state) => {
            if (state == this.diagnostic.bluetoothState.POWERED_ON) {
                this.bluetoothOn = true;
            } else {
                this.bluetoothOn = false;
            }
        }).catch(error => {
            console.error(error);
            this.presentToast(error, 'top', 'danger');
        });

    }

    iniateScanning() {

        this.resetLog();
        this.writeLog('iniate scanning...');
        this.BLEdevices = [];
        this.isScanning = true;
        this.isFinishScanning = false;
        this.resetScanningResult();

        if (this.platform.is('desktop')) {
            this.scanDevice();
        } else {
            this.diagnostic.getBluetoothState().then((state) => {
                if (state == this.diagnostic.bluetoothState.POWERED_ON) {
                    this.bluetoothOn = true;
                    this.scanDevice();
                } else {
                    this.bluetoothOn = false;
                    this.gettingDevices = false;
                    this.isScanning = false;
                }
            }).catch(error => {
                console.error(error);
                this.gettingDevices = false;
                this.isScanning = false;
            });
        }
    }

    scanDevice() {

        this.writeLog('start scanning devices...');

        if (this.platform.is('android') || this.platform.is('ios')) {

            // use new  features
            this.scanBLE();

            // const unPair = [];
            // this.bluetoothSerial.discoverUnpaired().then((success) => {
            //     success.forEach((value, key) => {
            //         let exists = false;
            //         unPair.forEach((val2, i) => {
            //             if (value.id === val2.id) {
            //                 exists = true;
            //             }
            //         });
            //         if (exists === false && value.id !== '') {
            //             unPair.push(value);
            //         }
            //     });
            //     if (unPair) {
            //         this.unpairedDevices = unPair;
            //     } else {
            //         this.unpairedDevices = [];
            //     }
            //     this.gettingDevices = false;
            // }, (err) => {
            //     console.log(err);
            //     this.data.log += '<br>debug ' + err;
            //     this.gettingDevices = false;
            // });
        }

        if (this.platform.is('desktop')) {
            setTimeout(() => {
                this.BLEdevices = [
                    {
                        id: 121414412,
                        name: 'Testing device'
                    }
                ];
                this.isScanning = false;
                this.isFinishScanning = true;
            }, 2000);
        }

        // old code to get BLE or paired devices
        // this.bluetoothSerial.list().then((success) => {
        //     this.pairedDevices = success;
        //     this.gettingPairedDevices = false;
        //     this.data.log += JSON.stringify(success);
        // }, (err) => {
        //     this.gettingPairedDevices = false;
        //     console.log('error scan device', err);
        //     this.writeLog('debug: ' + err);
        // });
    }

    async selectDevice(device: any) {

        const alert = await this.alertController.create({
            header: 'Connect with ' + device.name + '?',
            message: 'Please make sure you are standing infront of the device.',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: () => {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Connect',
                    handler: () => {
                        this.writeLog('Device ID: ' + device.id);

                        if (this.platform.is('desktop')) {
                            this.deviceConnected();
                        } else if (this.platform.is('ios')) {
                            this.bluetoothSerial.connect(device.id).subscribe(this.bluetoothSuccess, this.bluetoothFail);
                        } else {
                            this.BleConnect(device);
                        }
                    }
                }
            ]
        });
        await alert.present();
    }

    bluetoothSuccess = (data) => {
        this.writeLog('Success: ' + data);
        this.deviceConnected();

        // force disconnect if too long - 10 sec
        setTimeout(() => {
            this.disconnectScanner();
        }, 10000);
    }
    bluetoothFail = (error) => {
        this.presentToast(error, 'top', 'danger');
        this.writeLog('Error: ' + error);
    }

    deviceConnected() {

        this.writeLog('Check device connection...');
        this.presentLoading();

        if (this.platform.is('desktop')) {
            const temp = 't36.68a29.89d29';
            this.handleData(temp);

            setTimeout(() => {
                this.disconnectScanner();
            }, 200);
        } else {
            let scanCount = 0;
            this.bluetoothSerial.subscribe('\n').subscribe(success => {

                this.writeLog('Subscribe data...');

                // increase the number to increase number of scan before disconnect
                if (scanCount < 1) {
                    this.handleData(success);
                    scanCount++;
                    this.presentToast('Successfully scan', 'top', 'success');
                } else {
                    this.disconnectScanner();
                }

            }, error => {
                this.writeLog('Error: fail reading data - ' + error);

                this.presentToast('Scanning failed, please try again.', 'top', 'danger');
                this.disconnectScanner();
            });
        }
    }

    handleData(data) {
        this.data.log += '<br/>Result: ' + data;
        this.writeLog('Result: ' + data);

        if (data && data.length > 0) {
            this.scannerResult.body_temperature = this.getBodyTemperature(data);
            this.scannerResult.ambient_temperature = this.getAmbientTemperature(data);
            this.scannerResult.scanner_distance = this.getScannerDistance(data);
            this.question.temperature = Number(this.scannerResult.body_temperature);
            this.parentPage.screeningForm.patchValue({
                temperature: this.question.temperature
            });

            if (!this.scannerResult.body_temperature || this.scannerResult.body_temperature <= 30) {
                this.presentToast('Failed to read data, please try again.', 'top', 'danger');
                this.resetScanningResult();
            } else {
                this.parentPage.scrollToBottomOnInit();
            }

        } else {
            this.presentToast('Scanning failed, please try again.', 'top', 'danger');
            this.resetScanningResult();
        }

    }

    resetScanningResult() {
        this.scannerResult.body_temperature = null;
        this.scannerResult.ambient_temperature = null;
        this.scannerResult.scanner_distance = null;
        this.question.temperature = null;
    }
    getBodyTemperature(word) {
        return Number(word.substring(
            word.lastIndexOf('t') + 1,
            word.lastIndexOf('a')
        ));
    }
    getAmbientTemperature(word) {
        return Number(word.substring(
            word.lastIndexOf('a') + 1,
            word.lastIndexOf('d')
        ));
    }
    getScannerDistance(word) {
        return Number(word.split('d')[1]);
    }

    async disconnect() {
        const alert = await this.alertController.create({
            header: 'Disconnect?',
            message: 'Do you want to Disconnect?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: () => {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Disconnect',
                    handler: () => {
                        this.disconnectScanner();
                    }
                }
            ]
        });
        await alert.present();
    }
    disconnectScanner() {

        this.dismissLoading();
        this.writeLog('Force disconnected');

        this.bluetoothSerial.disconnect().then((buffer) => {
            this.presentToast('Device disconnected', 'bottom', 'warning');
        }, (error) => {
            console.log(error);
        });

    }

    async presentToast(text, position = 'top', color = 'light') {

        let toast;
        if (position === 'top') {
            toast = await this.toastController.create({
                message: text,
                mode: 'ios',
                duration: 2000,
                position: 'top',
                color,
            });
        } else if (position === 'middle') {
            toast = await this.toastController.create({
                message: text,
                mode: 'ios',
                duration: 2000,
                position: 'middle',
                color,
            });
        } else {
            toast = await this.toastController.create({
                message: text,
                mode: 'ios',
                duration: 2000,
                position: 'bottom',
                color,
            });
        }
        toast.present();
    }
    async presentLoading() {
        this.loading = await this.loadingController.create({
            message: 'Please wait...'
        });
        await this.loading.present();
    }
    dismissLoading() {

        if (this.loading) {
            this.loading.dismiss();
        } else {

            setTimeout(() => {
                if (this.loading) {
                    this.loading.dismiss();
                }
            }, 500);

        }
    }

    // Below if new function for BLE on android or IOS if some function above cant be use

    scanBLE() {
        this.setStatus('Scanning for Bluetooth LE Devices');

        this.BLEdevices = []; // clear list
        this.ble.scan([], 5).subscribe(
            device => this.onDeviceDiscoveredBLE(device),
            error => this.scanErrorBLE(error)
        );

        setTimeout(() => {
            this.isScanning = false;
            this.isFinishScanning = true;
            this.setStatus.bind(this);
        }, 5000, 'Scan complete');
    }

    onDeviceDiscoveredBLE(device) {
        console.log('Discovered ' + JSON.stringify(device, null, 2));
        this.ngZone.run(() => {
            this.BLEdevices.push(device);
        });
    }

    // If location permission is denied, you'll end up here
    async scanErrorBLE(error) {
        this.isScanning = false;
        this.isFinishScanning = true;
        console.log(error);
        this.setStatus('Error, ' + error);
        this.presentToast(error, 'top', 'danger');

        this.isScanning = false;
        this.isFinishScanning = true;
    }

    setStatus(message) {
        console.log(message);
        this.ngZone.run(() => {
            this.statusMessage = message;
        });
    }

    async BLEselect(device: any) {

        const alert = await this.alertController.create({
            header: 'Connect with ' + device.name + '?',
            message: 'Please make sure you are standing infront of the device.',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: () => {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Connect',
                    handler: () => {
                        this.data.log += '<br>Device ID: ' + device.id;
                        this.BleConnect(device);
                    }
                }
            ]
        });
        await alert.present();
    }

    BleConnect(device) {
        this.writeLog('connecting BLE...');
        this.presentLoading();
        this.ble.connect(device.id).subscribe(
            peripheral => this.onConnected(peripheral),
            peripheral => this.onDeviceDisconnected(peripheral)
        );
    }

    BleDisconnect() {

        if (!this.peripheral) {
            this.writeLog2('No device connected.');
            return;
        }

        this.ble.stopNotification(
            this.peripheral.id,
            'FFE0',
            'FFE1'
        ).then((buffer) => {
            console.log('Stop notification.');
            this.writeLog2('Notification stopped.');
        }, (error) => {
            console.log('Error stop notification.');
            this.writeLog2('Error stop notification.');
        });

        this.ble.disconnect(this.peripheral.id).then(() => {
            console.log('Disconnected ' + JSON.stringify(this.peripheral));
            this.writeLog2('force disconnected');
            this.presentToast('Device disconnected', 'bottom', 'warning');
        }, () => {
            this.writeLog2('Error force disconnected');
            console.log('ERROR disconnecting ' + JSON.stringify(this.peripheral));
        });
    }

    onConnected(peripheral) {
        this.presentToast('Device connected', 'top', 'danger');
        this.writeLog('device connected');
        console.log(peripheral);
        this.dismissLoading();
        this.ngZone.run(() => {
            this.setStatus('');
            this.peripheral = peripheral;

            // this.BleRead();
            this.BleWrite();
        });
    }

    onDeviceDisconnected(peripheral) {
        this.writeLog('device disconnected - 10s');
        this.dismissLoading();
        this.presentToast('The device unexpectedly disconnected', 'top', 'danger');
    }

    BleWrite() {

        this.writeLog('start writeWithoutResponse');

        // Subscribe for notifications when the resitance changes
        const inputdata = new Uint8Array(3);
        inputdata[0] = 0x53; // S
        inputdata[1] = 0x54; // T
        inputdata[2] = 0x0a; // LF

        this.ble
            .writeWithoutResponse(
                this.peripheral.id,
                'FFE0',
                'FFE1',
                inputdata.buffer
            )
            .then(
                data => {

                    console.log(data);
                    this.BleSubscribe();

                },
                err => {
                    this.BleDisconnect();
                    this.writeLog('error writeWithoutResponse: ' + err);
                    console.log(err);
                }
            );
    }

    BleSubscribe() {

        this.writeLog2('start notification');

        setTimeout(() => {
            this.BleDisconnect();
        }, 5000);

        this.ble.startNotification(
            this.peripheral.id,
            'FFE0',
            'FFE1'
        ).subscribe(
            buffer => {
                const data = this.bytesToString(buffer);
                this.writeLog2('data: ');
                this.writeLog2(data);
                console.log(data);

                let scanCount = 0;
                // increase the number to increase number of scan before disconnect
                if (scanCount < 1) {
                    this.handleData(data);
                    scanCount++;
                    this.presentToast('Successfully scan', 'top', 'success');
                } else {

                    setTimeout(() => {
                        this.BleDisconnect();
                    }, 3000);

                    setTimeout(() => {
                        this.BleDisconnect();
                    }, 4000);

                    setTimeout(() => {

                        this.ble.isConnected(this.peripheral.id).then(
                            data => {
                                this.BleDisconnect();
                            },
                            err => {
                                this.BleDisconnect();
                            }
                        );

                    }, 5000);
                }

            }, (error) => {
                this.BleDisconnect();
                this.writeLog2('error notification: ' + error);
                this.presentToast('Unexpected Error, Failed to subscribe for changes, please try to re-connect.');
            }
        );
    }

    BleRead() {
        console.log('Reading BLE...');
        this.writeLog('Reading BLE...');

        this.ble.read(
            this.peripheral.id,
            this.peripheral.characteristics[0].service,
            this.peripheral.characteristics[0].characteristic
        ).then((buffer) => {
            const data = new Uint8Array(buffer);
            const stringBuffer = String.fromCharCode.apply(null, data);
            console.log('BLE read success', stringBuffer);
            this.writeLog('Buffer: ' + buffer);
            this.writeLog('stringBuffer: ' + stringBuffer);

            // this.handleData(stringBuffer);
            // this.BleDisconnect();
        }, (error) => {
            console.log('BLE read error', error);
            this.writeLog('BLE read error: ' + error);
        });

    }

    // ASCII only
    stringToBytes(stringBuffer) {
        const array = new Uint8Array(stringBuffer.length);
        for (let i = 0, l = stringBuffer.length; i < l; i++) {
            array[i] = stringBuffer.charCodeAt(i);
        }
        return array.buffer;
    }

    // ASCII only
    bytesToString(buffer) {
        return String.fromCharCode.apply(null, new Uint8Array(buffer));
    }

}

