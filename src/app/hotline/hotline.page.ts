import { CallNumber } from '@ionic-native/call-number/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  Marker,
  GoogleMapsAnimation,
  MyLocation,
  LatLngBounds,
  Poly
} from '@ionic-native/google-maps';
import { Component, OnInit } from '@angular/core';
import {
  ToastController,
  Platform
} from '@ionic/angular';

@Component({
  selector: 'app-hotline',
  templateUrl: 'hotline.page.html',
  styleUrls: ['hotline.page.scss'],
})
export class HotlinePage {

  map: GoogleMap;
  address: string;

  // GORYOKAKU_POINTS = [
  //   { lat: 5.1506, lng: 100.4845 },
  //   { lat: 5.158001, lng: 100.494427 },
  //   { lat: 5.148140, lng: 100.501572 },
  //   { lat: 5.140719, lng: 100.491543 },
  // ];
  // GORYOKAKU_POINTS = [
  //   { lat: 2.9386854, lng: 101.5923393 },
  //   // { lat: 2.9395294, lng: 101.6063023 },
  //   { lat: 2.933446, lng: 101.603810 },
  //   { lat: 2.930591, lng: 101.607477 },
  //   { lat: 2.925427, lng: 101.597489 },
  // ];

  campusFences = [
    {
      name: 'Main Campus',
      coordinate: [
        { lat: 5.3550, lng: 100.2931 },
        { lat: 5.3580, lng: 100.2931 },
        { lat: 5.3580, lng: 100.2880 },
        { lat: 5.3550, lng: 100.2880 },
      ],
      marker: null
    },
    {
      name: 'RST (Main Campus)',
      coordinate: [
        { lat: 5.3520, lng: 100.3105 },
        { lat: 5.3630, lng: 100.3105 },
        { lat: 5.3630, lng: 100.2931 },
        { lat: 5.3520, lng: 100.2931 },
      ],
      marker: null
    },
    {
      name: 'sains@usm',
      coordinate: [
        { lat: 5.338610, lng: 100.281504 },
        { lat: 5.340378, lng: 100.283274 },
        { lat: 5.341649, lng: 100.282249 },
        { lat: 5.340170, lng: 100.280082 },
      ],
      marker: null
    },
    {
      name: 'Engineering Campus',
      coordinate: [
        { lat: 5.1506, lng: 100.4845 },
        { lat: 5.158001, lng: 100.494427 },
        { lat: 5.148140, lng: 100.501572 },
        { lat: 5.140719, lng: 100.491543 },
      ],
      marker: null
    },
    {
      name: 'IPPT Campus',
      coordinate: [
        { lat: 5.5292, lng: 100.4752 },
        { lat: 5.5199, lng: 100.4752 },
        { lat: 5.5199, lng: 100.479638 },
        { lat: 5.5292, lng: 100.479638 },
      ],
      marker: null
    },
    {
      name: 'sains@bertam',
      coordinate: [
        { lat: 5.515024, lng: 100.439905 },
        { lat: 5.515024, lng: 100.438869 },
        { lat: 5.515995, lng: 100.438869 },
        { lat: 5.515995, lng: 100.439905 },
      ],
      marker: null
    },
    {
      name: 'Health Campus',
      coordinate: [
        { lat: 6.094298, lng: 102.278710 },
        { lat: 6.101690, lng: 102.278710 },
        { lat: 6.101690, lng: 102.2920 },
        { lat: 6.094298, lng: 102.2920 },
      ],
      marker: null
    },
  ];

  constructor(
    private callNumber: CallNumber,
    private geolocation: Geolocation,
    public toastCtrl: ToastController,
    private platform: Platform,
  ) {

  }

  ngOnInit() {
    // Since ngOnInit() is executed before `deviceready` event,
    // you have to wait the event.
    this.platform.ready().then(() => {
      // this.loadMap();
    });
  }

  loadMap() {

    console.log('load map');

    this.map = GoogleMaps.create('map_canvas', {
      gestures: {
        scroll: false,
        tilt: false,
        rotate: false,
        zoom: false
      },
      camera: {
        target: this.campusFences[0].coordinate
      },
      // camera: {
      //   target: {
      //     lat: 5.3550,
      //     lng: 100.2931
      //   },
      //   zoom: 15,
      //   tilt: 30
      // }
    });

    this.campusFences.forEach(campus => {

      let polygon = this.map.addPolygon({
        points: campus.coordinate,
        strokeColor: '#AA00FF',
        strokeWidth: 5,
        fillColor: '#880000'
      });

      campus.marker = this.map.addMarkerSync({
        position: (new LatLngBounds(campus.coordinate)).getCenter(),
        draggable: true,
        title: campus.name
      });

      campus.marker.showInfoWindow();

    });

    // marker.on(GoogleMapsEvent.MARKER_DRAG_END).subscribe(() => {
    //   console.log('changed');
    //   let position = marker.getPosition();
    //   let contain = Poly.containsLocation(position, this.campusEngineeringFence);
    //   // marker.setIcon(contain ? 'blue' : 'red');
    // });

    // marker.on('position_changed', function () {
    //     console.log('changed');
    //     let position = marker.getPosition();
    //     let contain = Poly.containsLocation(
    //       position, this.GORYOKAKU_POINTS);
    //       marker.setIcon(contain ? 'blue' : 'red');
    //   });

    //   marker.trigger('position_changed', 0, marker.getPosition());

    // this.goToMyLocation();
  }

  goToMyLocation() {
    this.map.clear();

    // Get the location of you
    this.map.getMyLocation().then((location: MyLocation) => {
      console.log(location.latLng.lat);
      console.log(location.latLng.lng);

      let insideMap = Poly.containsLocation(location.latLng, this.campusFences[0].coordinate);

      console.log('insideMap', insideMap);
      location.latLng.lat = 2.9386854;
      location.latLng.lng = 101.5923393;

      // Move the map camera to the location with animation
      this.map.animateCamera({
        target: location.latLng,
        zoom: 17,
        duration: 5000
      });

      // add a marker
      let marker: Marker = this.map.addMarkerSync({
        title: '@ionic-native/google-maps plugin!',
        snippet: 'This plugin is awesome!',
        position: location.latLng,
        animation: GoogleMapsAnimation.BOUNCE
      });

      // show the infoWindow
      marker.showInfoWindow();

      // If clicked it, display the alert
      marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {
        this.showToast('clicked!');
      });

      this.map.on(GoogleMapsEvent.MAP_READY).subscribe(
        (data) => {
          console.log('Click MAP', data);
        }
      );
    })
      .catch(err => {
        // this.loading.dismiss();
        this.showToast(err.error_message);
      });
  }

  async showToast(message: string) {
    let toast = await this.toastCtrl.create({
      message,
      duration: 2000,
      position: 'middle'
    });
    toast.present();
  }

  ionViewDidEnter(): void {

    // testing only
    this.checkLocation();
  }

  checkLocation() {

    this.geolocation.getCurrentPosition().then((resp) => {

      console.log(resp);

      let latitude = resp.coords.latitude;
      let longitude = resp.coords.longitude;

    }, error => {
      console.log('getCurrentPosition error: ', error);
    });

  }

  callNow(phoneNumber) {

    window.open(`tel:${phoneNumber}`, '_system');

    // this.callNumber.callNumber(phoneNumber, true)
    //   .then(res => console.log('Launched dialer!', res))
    //   .catch(err => console.log('Error launching dialer', err));
  }

}
