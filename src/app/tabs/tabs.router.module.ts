import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { TabsPage } from './tabs.page';
import { TrackerPage } from './../tracker/tracker.page';
import { ScreeningPage } from './../screening/screening.page';
import { HotlinePage } from './../hotline/hotline.page';

const routes: Routes = [
  {
    path: '',
    component: TabsPage,
    children: [
      {
        path: 'tracker',
        children: [
          {
            path: '',
            component: TrackerPage,
          },
        ]
      },
      {
        path: 'screening',
        children: [
          {
            path: '',
            component: ScreeningPage,
          },
        ]
      },
      {
        path: 'hotline',
        children: [
          {
            path: '',
            component: HotlinePage,
          },
        ]
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule { }
