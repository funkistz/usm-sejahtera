import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TabsPageRoutingModule } from './tabs.router.module';

import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

import { TabsPage } from './tabs.page';
import { TrackerPage } from './../tracker/tracker.page';
import { ScreeningPage } from './../screening/screening.page';
import { ComponentsModule } from '../components/components.module';
import { HotlinePage } from './../hotline/hotline.page';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    TabsPageRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ComponentsModule,
  ],
  declarations: [
    TabsPage,
    TrackerPage,
    ScreeningPage,
    HotlinePage,
  ]
})
export class TabsPageModule { }
