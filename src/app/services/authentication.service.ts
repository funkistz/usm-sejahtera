import { Injectable } from '@angular/core';
import * as firebase from 'firebase/app';
require('firebase/auth');
import { environment } from '../../environments/environment';
import { LocationService, User } from 'src/app/services/location.service';

@Injectable()
export class AuthenticateService {

    constructor(
        private locationService: LocationService,
    ) {

        if (!firebase.apps.length) {
            firebase.initializeApp(environment.firebase);
        }
    }

    registerUser(value) {
        return new Promise<any>((resolve, reject) => {
            firebase.auth().createUserWithEmailAndPassword(value.email, value.password)
                .then(
                    res => {

                        delete value.confirm_password;
                        delete value.password;
                        delete value.agreement;
                        value.register_date = new Date();

                        console.log('success register create user');
                        this.locationService.addUser(value).then(() => {
                            console.log('success create user');
                            resolve(res);
                        }, err => {
                            console.log(err);
                            reject(err);
                        });
                    },
                    err => reject(err)
                );
        });
    }

    loginUser(value) {
        return new Promise<any>((resolve, reject) => {
            firebase.auth().signInWithEmailAndPassword(value.email, value.password)
                .then(
                    res => resolve(res),
                    err => reject(err));
        });
    }

    isEmailVerified() {
        let user = firebase.auth().currentUser;

        if (user) {
            return user.emailVerified === true;
        }

        return false;
    }

    sendVerificationEmail() {
        return new Promise<any>((resolve, reject) => {
            firebase.auth().currentUser.sendEmailVerification()
                .then(
                    res => resolve(res),
                    err => reject(err));
        });
    }

    sendReset(value) {

        return new Promise<any>((resolve, reject) => {
            firebase.auth().sendPasswordResetEmail(value.email)
                .then(
                    res => resolve(res),
                    err => reject(err)
                );
        });
    }

    logoutUser() {
        return new Promise((resolve, reject) => {
            if (firebase.auth().currentUser) {
                firebase.auth().signOut()
                    .then(() => {
                        console.log('LOG Out');
                        resolve();
                    }).catch((error) => {
                        reject();
                    });
            }
        });
    }

    userDetails() {
        return firebase.auth().currentUser;
    }
}
