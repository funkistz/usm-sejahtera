import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument, DocumentReference } from '@angular/fire/firestore';
import { map, take } from 'rxjs/operators';
import { Observable } from 'rxjs';
import * as firebase from 'firebase/app';
import * as moment from 'moment';

export interface User {
  id?: string;
  name: string;
  department: string;
  email: string;
  phone: string;
  type: string;
  location: firebase.firestore.GeoPoint;
  first_screening_date: Date;
  last_screening_date: Date;
  temperature: number;
  ic: string;
  age: number;
  last_screening_campus: string;
  last_update: Date;
}

@Injectable({
  providedIn: 'root'
})
export class LocationService {
  private users: Observable<User[]>;
  private userCollection: AngularFirestoreCollection<User>;

  constructor(private afs: AngularFirestore) {
    this.userCollection = this.afs.collection<User>('users');
    this.users = this.userCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
  }

  getUsers(): Observable<User[]> {
    return this.users;
  }

  getUser(id: string): Observable<User> {
    return this.userCollection.doc<User>(id).valueChanges().pipe(
      take(1),
      map(user => {
        user.id = id;

        console.log('getUser', user);
        return user;
      })
    );
  }

  addUser(user): Promise<void> {
    return this.userCollection.doc(user.email).set(user, { merge: true });
  }

  updateUser(user: User): Promise<void> {
    return this.userCollection.doc(user.id).update(
      {
        name: user.name,
        phone: user.phone,
        ic: user.ic,
        department: user.department,
        age: user.age,
        last_update: new Date()
      });
  }

  updateLocation(user: User, location): Promise<void> {

    const point = new firebase.firestore.GeoPoint(location.lat, location.lng);

    return this.userCollection.doc(user.id).update(
      {
        location: point,
        last_sync: new Date(),
      });
  }

  updateAppStatus(id, status): Promise<void> {

    return this.userCollection.doc(id).update(
      {
        status
      });
  }

  addOrUpdateUser(user: User): Promise<void> {
    return this.userCollection.doc(user.id).set(
      {
        name: user.name
      }, { merge: true });
  }

  addUserLocations(user: User, location): Promise<void> {

    const point = new firebase.firestore.GeoPoint(location.lat, location.lng);
    let sec = 0;
    if (parseInt(moment().format('s')) > 30) {
      sec = 30;
    }
    const locationKey = moment().format('YYYYMMDD-HH:mm:' + sec);
    // const locationCol = 'location-' + moment().format('YYYYMMDD');

    return this.userCollection.doc(user.id).collection('locations').doc(locationKey).set(
      {
        location: point,
        last_sync: new Date()
      }, { merge: true });
  }

  updateScreeningStatus(user: User, screening): Promise<void> {

    const screeningKey = moment().format('YYYYMMDD');
    // const locationCol = 'location-' + moment().format('YYYYMMDD');
    const point = new firebase.firestore.GeoPoint(screening.latitude, screening.longitude);

    return this.userCollection.doc(user.id).collection('screenings').doc(screeningKey).set(
      {
        screening_question1: screening.screening_question1,
        screening_question2: screening.screening_question2,
        fever: screening.fever,
        cough: screening.cough,
        sore_throat: screening.sore_throat,
        running_nose: screening.running_nose,
        shortness_of_breath: screening.shortness_of_breath,
        temperature: screening.temperature,
        last_screening_campus: screening.campus,
        date_add: new Date(),
        location: point
      }, { merge: true });
  }

  updateScreeningStatusUser(user: User, screening): Promise<void> {

    const point = new firebase.firestore.GeoPoint(screening.latitude, screening.longitude);

    if (user.first_screening_date) {

      return this.userCollection.doc(user.id).update(
        {
          temperature: screening.temperature,
          last_screening_campus: screening.campus,
          last_screening_date: new Date(),
          last_screening_location: point,
        });
    } else {

      user.first_screening_date = new Date();

      return this.userCollection.doc(user.id).update(
        {
          temperature: screening.temperature,
          last_screening_campus: screening.campus,
          last_screening_date: new Date(),
          first_screening_date: new Date(),
          last_screening_location: point
        });
    }

  }
}
