import { Injectable } from '@angular/core';
import * as firebase from 'firebase/app';
require('firebase/auth');
import { environment } from '../../environments/environment';
import { LocationService, User } from 'src/app/services/location.service';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';
import {
    BackgroundGeolocation,
    BackgroundGeolocationConfig,
    BackgroundGeolocationResponse,
    BackgroundGeolocationEvents
} from '@ionic-native/background-geolocation/ngx';
import { Platform } from '@ionic/angular';
import { interval } from 'rxjs';

@Injectable()
export class TrackerService {

    locationOn: any = false;
    locationCoords: any;
    timetest: any;
    map;
    checkingBL;

    constructor(
        private locationService: LocationService,
        private diagnostic: Diagnostic,
        private androidPermissions: AndroidPermissions,
        private geolocation: Geolocation,
        private locationAccuracy: LocationAccuracy,
        private backgroundGeolocation: BackgroundGeolocation,
        public platform: Platform,
    ) {


    }

    diagnosticLocation(parentPage) {
        this.locationOn = false;
        // in 3 seconds do something
        this.diagnostic.isLocationAvailable().then(this.locationSuccess, this.locationFail);
    }

    locationSuccess = (data) => {
        this.checkLocationState();

        const intervalLC = interval(3000).subscribe(x => {

            console.log('checking location...');

            if (this.locationOn === false) {
                this.checkLocationState();
            }
        });
    }
    locationFail = (error) => {
        console.log(error);
        if (this.platform.is('desktop')) {
            this.locationOn = true;
        }
    }

    checkLocationState() {

        console.log('checking location state...');
        this.diagnostic.isLocationEnabled().then((success) => {

            if (success) {
                this.locationOn = true;
            } else {
                this.locationOn = false;
            }
        }, (error) => {
            console.log(error);
            this.locationOn = false;
        });
    }

    // Methos to get device accurate coordinates using device GPS
    getLocationCoordinates() {

        this.geolocation.getCurrentPosition();

        // this.geolocation.getCurrentPosition().then((resp) => {
        //     this.locationCoords.latitude = resp.coords.latitude;
        //     this.locationCoords.longitude = resp.coords.longitude;
        //     this.locationCoords.accuracy = resp.coords.accuracy;
        //     this.locationCoords.timestamp = resp.timestamp;
        // }).catch((error) => {
        //     alert('Error getting location' + error);
        // });
    }

}
