import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DepartmentsPageRoutingModule } from './departments-routing.module';

import { DepartmentsPage } from './departments.page';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { ComponentsModule } from '../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    DepartmentsPageRoutingModule,
    ComponentsModule,
  ],
  declarations: [DepartmentsPage]
})
export class DepartmentsPageModule { }
