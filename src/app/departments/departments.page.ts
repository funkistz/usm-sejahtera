import { Component } from '@angular/core';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { NavController } from '@ionic/angular';
import { BluetoothService } from '../bluetooth';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-departments',
  templateUrl: 'departments.page.html',
  styleUrls: ['departments.page.scss'],
})
export class DepartmentsPage {

  hello: any;
  departmentsOri = [];
  departments = [];

  constructor(
    private callNumber: CallNumber,
    public bluetoothService: BluetoothService,
    public navCtrl: NavController,
    public viewCtrl: ModalController,
  ) {

    this.parseJsonFile('./assets/json/kampus-induk.json', 'KAMPUS INDUK (PULAU PINANG)');
    this.parseJsonFile('./assets/json/kampus-kejuteraan.json', 'KAMPUS KEJURUTERAAN');
    this.parseJsonFile('./assets/json/kampus-kesihatan.json', 'KAMPUS KESIHATAN	');

  }

  handleInput(event) {

  }

  dismiss(department) {

    this.viewCtrl.dismiss(department);
  }

  parseJsonFile(url, name) {
    fetch(url).then(res => res.json())
      .then(json => {

        let departments: any = {};
        departments.name = name;
        departments.data = json;

        // departments.data = departments.data.map(a => {
        //   a.name = a.name.toLowerCase();
        //   return a;
        // });

        this.departmentsOri.push(departments);
        this.initializeItems();
      });
  }

  initializeItems() {
    this.departments = JSON.parse(JSON.stringify(this.departmentsOri));
  }

  searchDepartment(ev: any) {
    // Reset items back to all of the items
    this.initializeItems();

    // set val to the value of the searchbar
    const val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() !== '') {
      // this.isItemAvailable = true;

      this.departments.forEach(data => {

        data.data = data.data.filter((department) => {
          return (department.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
        });

      });


    } else {
      // this.isItemAvailable = false;
    }

    console.log(this.departmentsOri);
  }
}
