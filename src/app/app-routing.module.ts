import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

let routes: Routes = [
  { path: 'tabs', loadChildren: './tabs/tabs.module#TabsPageModule', },
  {
    path: '',
    loadChildren: () => import('./startup/startup.module').then(m => m.StartupPageModule)
  },
  { path: 'login', redirectTo: '', pathMatch: 'full' },
  {
    path: 'faq',
    loadChildren: () => import('./faq/faq.module').then(m => m.FaqPageModule)
  },
  {
    path: 'departments',
    loadChildren: () => import('./departments/departments.module').then(m => m.DepartmentsPageModule)
  },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
