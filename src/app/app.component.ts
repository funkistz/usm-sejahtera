import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { BackgroundMode } from '@ionic-native/background-mode/ngx';
import { LocationService, User } from 'src/app/services/location.service';
import { Environment } from '@ionic-native/google-maps';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private backgroundMode: BackgroundMode,
    private locationService: LocationService,
  ) {

    // const prefersDark = window.matchMedia('(prefers-color-scheme: dark)');
    // toggleDarkTheme(true);

    // function toggleDarkTheme(shouldAdd) {
    //   console.log('dark mode', shouldAdd);
    //   document.body.classList.toggle('dark', shouldAdd);
    // }

    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      setTimeout(() => {
        this.splashScreen.hide();
      }, 1000);

      // cordova-plugin-background-mode
      this.backgroundMode.on('activate').subscribe(() => {
        this.backgroundMode.disableWebViewOptimizations();
      });
      this.backgroundMode.on('deactivate').subscribe(() => {
      });
      this.backgroundMode.disableBatteryOptimizations();

      Environment.setEnv({
        // api key for server
        API_KEY_FOR_BROWSER_RELEASE: 'AIzaSyBjWck6rSgfq9DQc0rX5rthdA-AqPKGjGQ',

        // api key for local development
        API_KEY_FOR_BROWSER_DEBUG: 'AIzaSyBjWck6rSgfq9DQc0rX5rthdA-AqPKGjGQ'
      });

    });
  }
}
